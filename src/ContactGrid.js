import React, { Component } from 'react';
import './style.css';
import ContactList from './ContactList.js';

class ContactGrid extends React.Component {
  constructor(){
    super();
    this.state = {
      totalPages: 1,
      page: 1,
      inputValue: '',
      invalidInput: false,
      isPreviousArrowVisible: false,
      isNextArrowVisible: false,
      contacts: []
    }
  }

  downloadContacts(currentPage){
    var path = 'https://reqres.in/api/users?page=' + currentPage;
    fetch(path)
    .then(results => {
      return results.json()
    })
    .then(data => {
      this.setState({totalPages: data.total_pages});
      this.setState({contacts: data.data});
      this.setState({inputValue: currentPage});
      this.setState({page: currentPage});
      this.setState({isPreviousArrowVisible: currentPage > 1})
      this.setState({isNextArrowVisible: currentPage < data.total_pages})
    })
  }
  
  getContacts = ()=>{
    var page = 1;
    this.downloadContacts(page);
  }

  inputChanged(evt){
    var inputValue = evt.target.value;
    this.setState({inputValue: inputValue});
    if(evt.target.checkValidity()){
      if(inputValue === "" ){
        this.setState({contacts: []});
        this.setState({invalidInput: false});
      }
      else if(inputValue < 0 || inputValue > this.state.totalPages){
        this.setState({contacts: []});
        this.setState({invalidInput: true});
      }
      else{
        this.setState({invalidInput: false});
        this.downloadContacts(inputValue);
      }
    }
    else{
      this.setState({invalidInput: true});
      this.setState({contacts: []});
    }
  }

  previousPage = () =>{
    var currentPage = this.state.page;
    if(currentPage > 1)
    {
      var nextPage = --currentPage;
      this.setState({page: nextPage});
      this.downloadContacts(nextPage);
    }
  }

  nextPage = () =>{
    var currentPage = this.state.page;
    if(currentPage < this.state.totalPages)
    {
      var nextPage = ++currentPage;
      this.setState({page: nextPage});
      this.downloadContacts(nextPage);
    }
  }
  
  render() {
    return (
      <div>
        <div align="center" >
        <button onClick={this.getContacts}
              className="default-button rosario percentage-size">
            <i className="fa fa-download"></i>
                Get Contacts
        </button>
        </div>
        <div align="center">
          <button onClick={this.previousPage}
                  className={(this.state.isPreviousArrowVisible ? "" : "invisible ") + "default-button"}>
            <i className="fa fa-caret-left"></i>
          </button>
          <input id="pageInput" 
                className="default-input"
                value={this.state.inputValue}
                type="text" pattern="\d+"
                onChange={evt => this.inputChanged(evt)}></input>
          <button onClick={this.nextPage}
                  className={(this.state.isNextArrowVisible ? "" : "invisible ") + "default-button"}>
                  <i className="fa fa-caret-right"></i>
          </button>
        </div>
        <div align="center" >
          <label className={this.state.invalidInput ? "error-message" : "error-message hidden"}>Input is invalid</label>
        </div>
        <div align="center" >
          <ContactList contacts={this.state.contacts} />
        </div>
      </div>
    );
  }
}

export default ContactGrid;
