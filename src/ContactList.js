import React, { Component } from 'react';
import './style.css';
import ContactCard from './ContactCard.js';

class ContactList extends React.Component {
    render() {
      const rows = [];
      this.props.contacts.forEach((contact) => {
        rows.push(
          <ContactCard
            contact={contact}
            key={contact.id} />
        );
      });
  
      return (
        <ul align="center">
          {rows}
        </ul>
      );
    }
  }

  export default ContactList;